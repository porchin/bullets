using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Implement the following class. This class manages the game objects used as bullets in a game.
/// Assume that there will be a need to spawn a lot of bullets as efficiently as possible.
/// </summary>
public class BulletManagerTest : MonoBehaviour
{
	//Prefab for a single bullet
	public Bullet BulletPrefab;

    //TODO - declare the neccessary member variables
    public int InitialPoolSize;

    private readonly List<Bullet> objectPool = new List<Bullet>();

	void Start()
	{
        //TODO - system initialization
        InstantiateBulletsToPool(InitialPoolSize);
	}

	/// <summary>
	/// Spawn bullets
	/// </summary>
	/// <param name="count">number of bullets to spawn</param>
	/// <param name="aim">starting position and direction of the bullets</param>
	/// <param name="lifeTime">life time of bullets in seconds</param>
	/// <param name="force">how fast the bullets should travel per second</param>
	/// <param name="maxSpreadAngle">how wide the bullets can spread in degree</param>
	public void SpawnBullets(int count, Ray aim, float lifeTime, float force, float maxSpreadAngle)
	{
        //TODO - spawn the bullets according to the input parameters

        var bullets = objectPool.FindAll(x => !x.gameObject.activeSelf);
        if (bullets == null || bullets.Count < count)
        {
            bullets.AddRange(InstantiateBulletsToPool(count - bullets.Count));
        }

        for (var i = 0; i < bullets.Count; i++)
        {
            var bullet = bullets[i];

            bullet.gameObject.SetActive(true);

            bullet.transform.localPosition = aim.origin;
            bullet.transform.localRotation = Quaternion.LookRotation(aim.direction);

            var randomInCircle = Random.insideUnitCircle * maxSpreadAngle;
            bullet.transform.Rotate(new Vector3(randomInCircle.x, randomInCircle.y, 0f));

            bullet.Force = force;
            StartCoroutine(DestroyBullet(bullet.gameObject, lifeTime));
        }
	}

	private void UpdateBullets()
	{
        //TODO - update the position of the bullets

        var bullets = objectPool.FindAll(x => x.gameObject.activeSelf);
        for (int i = 0; i < bullets.Count; i++)
        {
            var bullet = bullets[i];
            bullet.transform.Translate(bullet.transform.rotation * Vector3.forward * bullet.Force * Time.deltaTime, Space.World);
        }
    }

	private void DrawBullets()
	{
		//TODO - visualize the bullets
	}

	void Update()
	{
		UpdateBullets();
		DrawBullets();

		//For testing
		if (Input.GetKeyDown(KeyCode.W))
		{
			SpawnBullets(50, new Ray(Vector3.zero, Vector3.forward), 3, 40, 10);
		}
		if (Input.GetKeyDown(KeyCode.S))
		{
			SpawnBullets(50, new Ray(Vector3.zero, Vector3.back), 3, 100, 10);
		}
	}

    private List<Bullet> InstantiateBulletsToPool(int count)
    {
        var bullets = new List<Bullet>();
        for (var i = 0; i < count; i++)
        {
            var bullet = InstantiateBulletToPool();
            bullets.Add(bullet);
        }
        return bullets;
    }

    private Bullet InstantiateBulletToPool()
    {
        var bullet = Instantiate(BulletPrefab, transform);
        bullet.gameObject.SetActive(false);
        objectPool.Add(bullet);

        return bullet;
    }

    private IEnumerator DestroyBullet(GameObject bullet, float lifeTime)
    {
        yield return new WaitForSeconds(lifeTime);
        bullet.SetActive(false);
    }
}
